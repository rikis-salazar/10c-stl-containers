# C++ STL Containers Summary

Information about containers specified in the current `C++` standard. The
contents of all files hosted here is the same. The only thing different is their
presentation (_e.g._ bullet points _vs_ slides _vs_ interpreted `markdown`).

## List of files

*   [Bullet points][bullet]

*   [Beamer Slides][slides]

*   [Bitbucket's markdown flavor][md]

[bullet]: 10c-containers.pdf
[slides]: 10c-containers_slides.pdf
[md]: 10c-containers.md

% C++ Standard Containers (Summary)
% Ricardo Salazar
% January 24, 2017


# STL Containers  

## From [`www.cplusplus.com/reference/stl`][ref-stl]

Containers

*   are **holders**; they store a collection of other objects.

*   are implemented as class templates.

*   manage the storage space for its elements and provide member functions to
    access them.

*   replicate structures very commonly used in programming:

    -   dynamic arrays (`vector`)
    -   queues (`queue`)
    -   stacks (`stack`)
    -   heaps (`priority_queue`)
    -   linked lists (`list`)
    -   trees (`set`)
    -   associative arrays (`map`)


## Classification

 1. **sequence containers**:

    > all elements can be accessed _sequentially_.

 1. **container adaptors**:
 
    > provide a specific interface relying on an object of other container
    > classes to handle the elements.

 1. **associative containers**:
 
    > implement _ordered associative arrays_; they store arbitrary elements
    > (provided that the _indices_ are _weakly ordered_).
    
 1. **unordered associative containers**:
 
    > implement _hash tables_; they store arbitrary elements (provided that
    > _indices_ can be _hashed_).

[ref-stl]: http://www.cplusplus.com/reference/stl


# Sequence containers

## General info

The current `C++` standard defines the following containers.

*   `array`: a compile-time non-resizable array.[^array]

*   `vector`: a resizable array with fast random access.

*   `deque`: a **d**ouble-**e**nded **que**ue with fast random access.

*   `list`: a doubly linked list.

*   `forward_list`: a singly linked list.[^forw-list]


## Properties

*   `array`, `vector` and `deque` support fast random access via `operator[]`.

*   `list` supports bidirectional iteration; `forward_list` supports only
    unidirectional iteration.

*   `array` does not support element insertion or removal.

*   `vector` supports fast insertion/removal at the end in most cases.

*   `deque` supports fast insertion/removal at both ends (front and back) in
    most cases.

*   `list`, `forward_list`, support fast insertion or removal of elements.


## Complexity

\begin{center}
    Average cost (container with $n$ elements)
\end{center}

| **Container** | insert[^average] | search[^unsorted] | rand access |
|:-------------|:-------------:|:------------:|:------------:|:------------:|
| `array` | `N/A` | $\mathcal{O}(n)$ | $\mathcal{O}(1)$ | 
| `vector` | $\mathcal{O}(n)$ | $\mathcal{O}(n)$ | $\mathcal{O}(1)$ | 
| `deque` | $\mathcal{O}(n)$ | $\mathcal{O}(n)$ | $\mathcal{O}(1)$ |
| `list` | $\mathcal{O}(1)$ | $\mathcal{O}(n)$ | `N/A` | 
| `forward_list` | $\mathcal{O}(1)$ | $\mathcal{O}(n)$ | `N/A` | 


# Container adaptors

## General info

Provide a specific interface based on an underlying container.

*   `stack` [Last-In, First-Out (LIFO) access]

    -   Default underlying container: `deque`, with cost $\mathcal{O}(1)$.
    -   Others can be used if they provide: `empty`, `size`, `back`,
        `push_back`, and `pop_back`.

*   `queue` [First-In, First-Out (FIFO) access]

    -   Default underlying container: `deque`, with cost $\mathcal{O}(1)$. 
    -   Others can be used if they provide: `empty`, `size`, `front`, `back`,
        `push_back`, and `pop_front`.

*   `priority_queue` [sorted-order access to elements]

    -   Elements must have a _weak order_ (_i.e.,_ `operator<`).
    -   Default underlying container: `vector`, with cost $\mathcal{O}(\log n)$.
    -   Others can be used if they provide **random access**, as well as
        `empty`, `size`, `front`, `push_back`, and `pop_back`.


# Associative containers

## General info

The current revision of the `C++` defines:

*   `set`.- an _ascending_ container of _unique_ elements.

*   `map`.- a sequence of key/value pairs; keys are _unique_ and they determine
    the _order_ of the sequence.

*   `multiset`.- like a `set`, but elements are not necessarily unique.

*   `multimap`.- like a `map`, but keys are not necessarily unique.

Roughly speaking, a `set` is a map where the key is the value itself.


## Properties

*   Only elements that provide a _weak order_ can be stored in associative
    containers.

*   Designed to be efficient in accessing elements by their key.[^seq-cont]

*   Guaranteed to perform insertion, deletion, and searches in
    $\mathcal{O}(\log n)$ time.

*   Typically implemented using _self-balancing binary search trees_.

*   Elements are inserted in a pre-defined order such as sorted ascending.

*   Support _bidirectional iterators_[^later].

# Unordered associative containers

## General info & properties

The current revision of the `C++` defines:

*   `unordered_set` 
*   `unordered_map`
*   `unordered_multiset`
*   `unordered_multimap`

They are similar to the associative containers but as their name implies, the
elements are not ordered.

*   They are implemented via _hash tables_.

*   Only elements that provide a _hash function_ can be stored.

*   Perform most operations $\mathcal{O}(1)$ time under right circumstances.

*   They only provide _forward iterators_.

[^array]: Since `C++11`.
[^forw-list]: Since `C++11`. Added as a space-efficient alternative to `list`.
[^average]: In the best case scenario the cost is $\mathcal{O}(1)$.
[^unsorted]: Assuming that elements are not _sorted_; or that they cannot be
    compared to each other.
[^seq-cont]: Sequence containers are efficient in accessing by position.
[^later]: We'll study iterators later in this course.
